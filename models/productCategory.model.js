const mongoose = require('mongoose');

const validator = require('mongoose-unique-validator');
const AutoIncrement = require('../lib/auto_increment')(mongoose);

mongoose.set('useCreateIndex', true);

const categorySchema = mongoose.Schema({
    "_id": Number,
    "categoryName": String,
    "description": String
}, { _id: false });


//Validate Data
categorySchema.plugin(validator);

//Add Auto Increament To Event ID
categorySchema.plugin(AutoIncrement, {
    modelName: 'categories',
    type: Number,
    unique: true,
    fieldName: '_id'
});

module.exports = mongoose.model('categories', categorySchema);