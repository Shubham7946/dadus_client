const mongoose = require('mongoose');

const validator = require('mongoose-unique-validator');
const AutoIncrement = require('../lib/auto_increment')(mongoose);

mongoose.set('useCreateIndex', true);

const sellerProductSchema = mongoose.Schema({
    "_id": Number,
    "distributorId": Number,
    "categoryId": Number,
    "subCategoryId": Number,
    "brandId": Number,
    "name": String,
    "lname": String,
    "shortInfo": String,
    "description": String,
    "specification": String,
    "shipmentDetails": [],
    "hasVariants": { type: Boolean, default: false },
    "options": Object,
    "variants": [{}],
    "isReturnAvail": { type: Boolean, default: false },
    "returnDays": { type: Number, default: 0 },
    "hasGST": { type: Boolean, default: true },
    "gstRate": { type: Number, default: 0 },
    "otherCharges": { type: Number, default: 0 },
    "deliveryCharges": { type: Number, default: 0 },
    "expiryDate": { type: Date, default: undefined },
    "images": Array,
    "new": { type: Boolean, default: false },
    "isActive": { type: Boolean, default: false },
    "bestSelling": { type: Boolean, default: false },
    "verifiedSeller": { type: Boolean, default: false },
    "enableDailyDeal": { type: Boolean, default: false },
    "assured": { type: Boolean, default: false },
    "isDeleted": { type: Boolean, default: false },
    "createdBy": { type: Number, default: 1 }
}, { strict: false });

//Validate Data
sellerProductSchema.plugin(validator);

sellerProductSchema.index({ lname: 'text', name: 'text' });

//Add Auto Increament To Event ID
sellerProductSchema.plugin(AutoIncrement, {
    modelName: 'sellerProduct',
    type: Number,
    unique: true,
    fieldName: '_id'
});
module.exports = mongoose.model('sellerproducts', sellerProductSchema);