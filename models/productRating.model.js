const mongoose = require('mongoose');

const validator = require('mongoose-unique-validator');
const AutoIncrement = require('../lib/auto_increment')(mongoose);

mongoose.set('useCreateIndex', true);

const ratingSchema = mongoose.Schema({
    "_id": Number,
    "ratedBy": Number,
    "productId": Number,
    "rating": Number,
    "review": String,
    "ratedAt": Date
}, { _id: false });

//Validate Data
ratingSchema.plugin(validator);

//Add Auto Increament To Event ID
ratingSchema.plugin(AutoIncrement, {
    modelName: 'sellerProductsRatings',
    type: Number,
    unique: true,
    fieldName: '_id'
});
module.exports = mongoose.model('sellerProductsRatings', ratingSchema);