const mongoose = require('mongoose');

const validator = require('mongoose-unique-validator');
const AutoIncrement = require('../lib/auto_increment')(mongoose);
const mongoosePaginate = require('mongoose-paginate-v2');
mongoose.set('useCreateIndex', true);

const userSchema = mongoose.Schema({
    _id: Number,
    "first_name": String,
    "last_name": String,
    "email": { type: String,},
    "contact": { type: String},
    "password": String
}, { _id: false });

//Validate Data
userSchema.plugin(validator);

//Add Auto Increament To Event ID
userSchema.plugin(AutoIncrement, {
    modelName: 'users',
    type: Number,
    unique: true,
    fieldName: '_id'
});

userSchema.plugin(mongoosePaginate);
//Middleware To Call Before Save
userSchema.pre('save', function(next){
    console.log("Saving User Details.....");
    next();
});


module.exports = mongoose.model('users', userSchema);