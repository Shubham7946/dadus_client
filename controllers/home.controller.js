const ProductModel = require('../models/product.model');

class HomeControlller{
    //Method To Render Home Page
    async renderHomePage(req, res){
        res.render("home/index");
    }

    //Method To Get All Products Count
    async getProductsCount(req, res){
        let productCount = await ProductModel.count();
        res.send({count: productCount});
    }

    //Method To Get Product List
    async getProductsList(req, res){
        let products = await ProductModel.aggregate([
            {
                $lookup: {
                    from: "sellerProductsRatings",
                    localField: "_id",
                    foreignField: "productId",
                    as: "rating"
                }
            },
            {$addFields: {rating: {$avg: "$rating.rating"}, ratingCount: {$size: "$rating"}}},
            {
                $project: {
                    rating: 1,
                    ratingCount: 1,
                    name: 1,
                    options: 1,
                    isAvail: 1,
                    isDiscountAvail: {$arrayElemAt: ["$variants.isDiscountAvail", 0]},
                    isGSTApplied: 1,
                    gstRate: 1,
                    discount: {$arrayElemAt: ["$variants.discount", 0]},
                    subTotal: {$arrayElemAt: ["$variants.totalPrice", 0]},
                    offerPrice: {
                        $sum: [
                            {$arrayElemAt: ["$variants.totalPrice", 0]},
                            {$multiply: ["$gstRate", {$arrayElemAt: ["$variants.totalPrice", 0]}, 0.01]}
                        ]
                    },
                    actualPrice: {
                        $sum: [
                            {$arrayElemAt: ["$variants.salePrice", 0]},
                            {$multiply: ["$gstRate", {$arrayElemAt: ["$variants.salePrice", 0]}, 0.01]}
                        ]
                    },
                    image: {$arrayElemAt: ["$images", 0]}

                }
            },
            {"$skip": parseInt(req.params.skip)},
            {"$limit": parseInt(req.params.limit)}

        ]);
        console.log("itm", products)
        res.send(products);
    }


}

module.exports = new HomeControlller();