const categorySchema = require('../models/productCategory.model');

class Home {
    //Method To Get All Categories
    async getAllCategories(req, res) {
        try {
            var categories = await categorySchema.find({}, { _id: 1, categoryName: 1 });
            res.send(categories);
        } catch (err) {
            res.sendStatus(500);
        }
    }

}

module.exports = new Home();