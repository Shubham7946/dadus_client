const itemSchema = require('../models/product.model');

const productRatingSchema = require('../models/productRating.model');

class ProductListController {
    //********************************* Methods For  Getting Products List *******************************//
    //Method to get items count
    async getItemsCount(req, res) {
        try {
            let searchObj = {isDeleted: false, categoryId: parseInt(req.params.catId)};
            console.log(searchObj)
            let itemCount = await itemSchema.aggregate([
                {$match: {isDeleted: false, categoryId: parseInt(req.params.catId)}},
                {
                    $count: "count"
                }
            ]);
            console.log("Count", itemCount);
            if (itemCount.length > 0)
                res.send({count: itemCount[0].count});
            else
                res.send({count: 0});
        } catch (err) {
            console.log(err)
            res.sendStatus(500);
        }
    }

    //Method to get items list
    async getItemsList(req, res) {
        try {
            console.log("TES")
            console.log(req.params.catId);
            let sortObj = {rating: -1, ratingCount: -1};
            let searchObj = {isDeleted: false, categoryId: parseInt(req.params.catId)};
            console.log(searchObj)
            let items = await itemSchema.aggregate([
                {$match: searchObj},
                {
                    $lookup: {
                        from: "sellerProductsRatings",
                        localField: "_id",
                        foreignField: "productId",
                        as: "rating"
                    }
                },
                {$addFields: {rating: {$avg: "$rating.rating"}, ratingCount: {$size: "$rating"}}},
                {
                    $project: {
                        rating: 1,
                        ratingCount: 1,
                        name: 1,
                        options: 1,
                        isAvail: 1,
                        isDiscountAvail: {$arrayElemAt: ["$variants.isDiscountAvail", 0]},
                        isGSTApplied: 1,
                        gstRate: 1,
                        discount: {$arrayElemAt: ["$variants.discount", 0]},
                        subTotal: {$arrayElemAt: ["$variants.totalPrice", 0]},
                        offerPrice: {
                            $sum: [
                                {$arrayElemAt: ["$variants.totalPrice", 0]},
                                {$multiply: ["$gstRate", {$arrayElemAt: ["$variants.totalPrice", 0]}, 0.01]}
                            ]
                        },
                        actualPrice: {
                            $sum: [
                                {$arrayElemAt: ["$variants.salePrice", 0]},
                                {$multiply: ["$gstRate", {$arrayElemAt: ["$variants.salePrice", 0]}, 0.01]}
                            ]
                        },
                        image: {$arrayElemAt: ["$images", 0]}

                    }
                },
                {$sort: sortObj},
                {"$skip": parseInt(req.params.skip)},
                {"$limit": parseInt(req.params.limit)}

            ]);
            console.log("itm", items)
            res.send(items);
        } catch (err) {
            console.log(err)
            res.sendStatus(500);
        }
    }




}

module.exports = new ProductListController();