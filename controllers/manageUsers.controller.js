const userSchema = require('../models/adminUsers.model');

class ManageUsers {

    //Method To Get All Users
    async getAllUsers(req, res, next) {
        try {
            var users = await userSchema.find();
            res.send(users);
        } catch (err) {
            res.sendStatus(500);
        }
    }

    //Method To Get User By Id
    async findById(req, res) {
        try {
            const user = await User.findById(req.params.id);
            if (!user) {
                res.sendStatus(404);
            }
            res.send(user);
        } catch (err) {
            if (err.name === 'CastError' || err.name === 'NotFoundError') {
                res.sendStatus(404);
            }
            res.sendStatus(500);
        }
    }

    //Method To Add New User
    async add(req, res) {
        console.log(req.body)
        try {
            const newUser = await new userSchema(req.body).save();
            res.sendStatus(201);
        } catch (err) {
            console.log(err)
            if (err.name == 'ValidationError') {
                var errorMessages = err.message.replace("users validation failed:", "");
                errorMessages = errorMessages.split(',');
                for (var i = 0; i < errorMessages.length; i++) {
                    errorMessages[i] = errorMessages[i].split(':')[1];
                }
                res.status(200).send({ message: errorMessages, error: true });

            }
            res.sendStatus(500);
        }
    }

    //Method To Edit User
    async update(req, res, next) {
        try {
            const user = await userSchema.findByIdAndUpdate(
                req.params.id,
                req.body, {
                    runValidators: true,
                    context: 'query'
                }
            );
            if (!user) {
                res.sendStatus(404);
            }
            res.sendStatus(201);
        } catch (err) {
            if (err.name == 'ValidationError') {
                var errorMessages = err.message.replace("Validation failed:", "");
                errorMessages = errorMessages.split(',');
                for (var i = 0; i < errorMessages.length; i++) {
                    errorMessages[i] = errorMessages[i].split(':')[1];
                }
                res.status(200).send({ message: errorMessages });
            } else {
                res.sendStatus(500);
            }
        }
    }

    //Method To Delete User
    async delete(req, res) {
        try {
            const user = await userSchema.findByIdAndRemove(req.params.id);
            if (!user) {
                res.sendStatus(404);
            }
            res.sendStatus(200);
        } catch (err) {
            console.log(err)
            if (err.name === 'CastError' || err.name === 'NotFoundError') {
                res.sendStatus(404);
            }
            res.sendStatus(500);
        }
    }
}

module.exports = new ManageUsers();