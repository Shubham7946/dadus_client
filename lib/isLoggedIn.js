module.exports = function (req, res, next) {
    if(req.session.menuItems == undefined){
        return res.redirect('/login/seller');
    }
    var url = "/"+req.route.path.split('/')[1];
    if (req.session.sessionAllowedUrls.indexOf(url) !== -1) {
        return next();
    }
    res.redirect('/login/seller');
};