const router = require('express').Router();
const mastersController = require('../controllers/masters.controller');
//city state country import
const dropdown = require('countrycitystatejson');

//Method To Get Countries List
router.get('/fetchCountries', function(req, res) {
    res.send(dropdown.getCountries());
});


//Method To Get Countries List
router.get('/fetchStates/:countryName', function(req, res) {
    res.send(dropdown.getStatesByShort(req.params.countryName));
});

//Method To Get Countries List
router.get('/fetchCities/:countryName/:stateName', function(req, res) {
    res.send(dropdown.getCities(req.params.countryName, req.params.stateName));
});

//Method To Fetch Category List
router.get('/manageCategorys/getCategorys', mastersController.getAllCategories)
module.exports = router;