const router = require('express').Router();

//**************************************** Methods For Home Page ***********************************/
const homeController = require('../controllers/home.controller');
//Method To Render Home Page
router.get('/',homeController.renderHomePage);


//Method To Get Products Count
router.get('/home/getProductListCount', homeController.getProductsCount);

//Method To Get Products List
router.get('/home/getProductsList/:limit/:skip/', homeController.getProductsList);

//**************************************** Methods For Products List Page ***********************************/
const productListController =  require('../controllers/productsList.controller');
//Method To Render Home Page
router.get('/productsList/:categoryID', function(req, res){
    res.render("productList/index", {categoryID: req.params.categoryID});
});

//Method To Get Items Count
router.post('/productsList/getItemsCount/:catId', productListController.getItemsCount);

//Method to get items list
router.post('/productsList/getItemsList/:limit/:skip/:catId', productListController.getItemsList);

module.exports = router;