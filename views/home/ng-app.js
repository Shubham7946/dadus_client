var app = angular.module("myApp", ['ngMessages', 'ui.bootstrap', 'ui.bootstrap.modal']);

app.factory('PagerService', PagerService);


app.controller('myAppController', function($scope, $http,PagerService) {
    $scope.dummyItems = _.range(0, 2); // dummy array of items to be paged
    $scope.pager = {};
    $scope.searchData = {};
    $scope.data = {};
    $scope.itemsList = [];
    $scope.setPage = setPage;
    //Variable to store page size
    $scope.pageSize = 2;

    $scope.initializeController = function(categoryID){
    
        $scope.loading = true;
    
        $scope.getAllCategories();
        init();
    };

    // //Method To Encrypt URL
    $scope.encryptUrl = function (id) {
        return btoa("product:"+ id);
    };




    //Method To Get All Categories
    $scope.getAllCategories = function() {
        $http.get('/manageCategorys/getCategorys').then(function(response) {
            console.log(response)
            if (response.status === 200) {
                $scope.categories = response.data;
            } else
                alertify.error("Failed To Load Categories");
        });
    };


    function init() {
        console.log("initController", $scope.data)
        $scope.itemsList = [];
        $scope.items = [];
        $scope.pager = {};
        // initialize to page 1
        $scope.loading = true;
        $http.get(`/home/getProductListCount`).then(function(response){
            console.log(response.data)
            let size = parseInt(response.data.count);
            $scope.dummyItems = _.range(0,size);
            $scope.setPage(1);
            $scope.loading = false;
        });
    }

    function setPage(page) {
        $("html, body").animate({ scrollTop: 650 }, "slow");
        if (page < 1 || page > $scope.pager.totalPages) {
            return;
        }

        // get pager object from service
        $scope.pager = PagerService.GetPager($scope.dummyItems.length, page, $scope.pageSize);

        let skip = ($scope.pager.currentPage-1) * $scope.pager.pageSize;

        let limit = $scope.pager.pageSize;

        if($scope.itemsList.length < (skip+1)){
            $scope.loading = true;
            $http.get(`/home/getProductsList/${limit}/${skip}`).then(function(response){
                $scope.itemsList.push(...response.data);
                $scope.items = response.data;
                $scope.loading = false;
            });
        }else{
            $scope.items = $scope.itemsList.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
        }
        // get current page of items
    }
});


//Paginator Factory Function
function PagerService() {

    // service definition
    let service = {};

    service.GetPager = GetPager;
    return service;

    // service implementation
    function GetPager(totalItems, currentPage, pageSize) {
        // default to first page
        currentPage = currentPage || 1;

        // default page size is 10
        pageSize = pageSize || 10;

        // calculate total pages
        let totalPages = Math.ceil(totalItems / pageSize);

        let startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // calculate start and end item indexes
        let startIndex = (currentPage - 1) * pageSize;
        let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

        // create an array of pages to ng-repeat in the pager control
        let pages = _.range(startPage, endPage + 1);

        // return object with all pager properties required by the view
        return {
            totalItems: totalItems,
            currentPage: currentPage,
            pageSize: pageSize,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            startIndex: startIndex,
            endIndex: endIndex,
            pages: pages
        };
    }
}